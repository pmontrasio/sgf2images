require "board"
require "encode_sgf"

RSpec.describe "encoded_position" do
  it "returns an empty string for an empty board" do
    board = Board.new(4)
    expect(encoded_position(board)).to eq("")
  end

  it "encodes a board" do
    text_board = ". O X .\n. O X X\nO O X .\n. O X ."
    board = Board.new(4, text_board)
    expect(encoded_position(board)).to eq("B4 c4 B3 c3 d3 A2 B2 c2 B1 c1")
  end
end
