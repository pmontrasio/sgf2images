#!/usr/bin/env ruby

# Usage: ./sgf2images.rb browser port file.sgf
# browser is the command that starts the browser
# port is the local TCP port where sgf2images makes the browser connect to
# file.sgf is the SGF file for the game to display
#
# Example: ./sgf2images firefox 8001 game.sgf
#
# It creates a directory named after the SGF file (output/game/) containing
# * One png per move, with a 3D view of the goban: NNN.png
# * One txt file per move, with a description of the position: NNN.txt

def use_gems_installed_from_git
  # https://stackoverflow.com/questions/36047575/unable-to-require-gem-from-git-with-rvm/57924225#57924225
  require "bundler"
  Bundler.setup
end

use_gems_installed_from_git

require "base64"
require "fileutils"
require "json"
require "sinatra"

require "goboard"

require_relative "lib/encode_sgf"
require_relative "lib/read_sgf"
require_relative "lib/play"

browser_command = ARGV[0]
port = ARGV[1].to_i
sgf_file_name = ARGV[2]

size, moves = read_sgf(sgf_file_name)
board = Board.new(size)
move = 0
output_directory = "output/#{sgf_file_name.gsub(/\.sgf$/, "")}"
FileUtils.mkdir_p(output_directory)

configure do
  set :port, port
  set :public_folder, "#{File.dirname(__FILE__)}/static"
end

get "/board" do
  content_type :json
  response = play(board, moves[move])
  move += 1
  prefix = "#{output_directory}/#{"%03d" % move}"
  board.to_text.split("\n").each do |row|
    p row
  end
  encoded_board = encoded_position(board)
  p encoded_board
  File.open("#{prefix}.txt", "wb") do |txt|
    txt.write(encoded_board)
  end
  response[:currentMove] = move
  response.to_json
end

post "/board" do
  request.body.rewind  # in case someone already read it
  data = JSON.parse request.body.read
  image = data["image"]
  image_move = data["currentMove"]
  prefix = "#{output_directory}/#{"%03d" % image_move}"
  File.open("#{prefix}.png", "wb") do |png|
    png.write(Base64.decode64(image))
  end
  {"result" => "ok"}.to_json
end

system("#{browser_command} http://localhost:#{port}/index.html")
