def play(board, move_description)
  type_of_play, play_description = move_description
  if type_of_play == :end
    {command: "close window"}
  else
    case type_of_play
    when :handicap
      number_of_stones = play_description
      Handicap.coordinates(number_of_stones).each do |row, column|
        board.black!(row, column)
      end
    when :white
      row, column = play_description
      board.white!(row, column)
    when :black
      row, column = play_description
      board.black!(row, column)
    when :end
    end
    {command: "display board", size: board.size, board: board.to_text}
  end
end

class Handicap
  def self.coordinates(number_of_stones)
    [
      [],
      [],
      [[4, 16], [16, 4]],
      [[4, 16], [10, 10], [16, 4]],
      [[4, 4], [4, 16], [16, 4], [16, 16]],
      [[4, 4], [4, 16], [10, 10], [16, 4], [16, 16]],
      [[4, 4], [4, 16], [10, 4], [16, 4], [10, 16], [16, 16]],
      [[4, 4], [4, 16], [10, 4], [10, 10], [16, 4], [10, 16], [16, 16]],
      [[4, 4], [4, 16], [10, 4], [4, 10], [16, 10], [16, 4], [10, 16], [16, 16]],
      [[4, 4], [4, 16], [10, 4], [4, 10], [10, 10], [16, 10], [16, 4], [10, 16], [16, 16]],
    ][number_of_stones]
  end
end
