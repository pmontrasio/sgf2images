# Pietre nere minuscole, bianche maiuscole, riga per riga da in alto a sinistra
#
# F17 d16 q16 d14 q6 d4 L4 q4 O3
#
# Origine in basso a sinistra

def encoded_position(board)
  stones = []
  board.to_text.split("\n").each_with_index do |row, row_index|
    row.split(" ").each_with_index do |intersection, column_index|
      next if intersection == "."
      coordinates = "#{("A".ord + column_index).chr}#{board.size - row_index}"
      if intersection == "O"
        stones << coordinates
      else
        stones << coordinates.downcase
      end
    end
  end
  stones.join(" ")
end
