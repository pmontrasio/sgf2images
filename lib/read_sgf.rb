require "sgf"

class SGFBoard
  COORDINATES = "A B C D E F G H J K L M N O P Q R S T"

  # https://senseis.xmp.net/?Coordinates%2FSGF
  ROW_COORDINATES = {
    "a" => 1, "b" => 2, "c" => 3, "d" => 4, "e" => 5, "f" => 6,
    "g" => 7, "h" => 8, "i" => 9, "j" => 10, "k" => 11, "l" => 12,
    "m" => 13, "n" => 14, "o" => 15, "p" => 16, "q" => 17, "r" => 18, "s" => 19
  }
  COLUMN_COORDINATES = {
    "a" => 19, "b" => 18, "c" => 17, "d" => 16, "e" => 15, "f" => 14,
    "g" => 13, "h" => 12, "i" => 11, "j" => 10, "k" => 9, "l" => 8,
    "m" => 7, "n" => 6, "o" => 5, "p" => 4, "q" => 3, "r" => 2, "s" => 1
  }

  def self.to_human_coordinates(sgf_coordinate)
    row = sgf_coordinate[0]
    column = sgf_coordinate[1]
    [ROW_COORDINATES[row], COLUMN_COORDINATES[column]]
  end

end # class SGFBoard

def read_sgf(file_sgf)
  sgf = SGF.parse(file_sgf)
  game = sgf.gametrees.first
  game_info = game.current_node
  size = game_info.properties.has_key?("SZ") ? game_info.properties["SZ"].to_i : 19
  moves = []
  if game_info.properties.has_key?("HA")
    moves = [:handicap, game_info.properties["HA"].to_i] # [:handicap, 2]
  end
  loop do
    node = game.next_node
    break unless node
    if node.properties.has_key?("W")
      moves << [:white, SGFBoard::to_human_coordinates(node["W"])] # [:white, [4, 5]]
    elsif node.properties.has_key?("B")
      moves << [:black, SGFBoard::to_human_coordinates(node["B"])] # [:black, [16, 15]]
    end
  end
  moves << [:end, :game]
  [size, moves]
end
