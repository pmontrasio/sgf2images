Local web app to convert a SGF file into a sequence of images.

# Usage

```
./sgf2images.rb browser port file.sgf
```

* `browser` is the command that starts the browser.
* `port` is the local TCP port where sgf2images makes the browser connect to.
* `file.sgf` is the SGF file of the game.

It creates a directory named after the SGF file (`output/game/`) containing
* One png per move, with a 3D view of the goban: NNN.png
* One txt file per move, with a description of the position: NNN.txt

# Example

```
./sgf2images.rb firefox 8001 game.sgf
```

To manually query the web app

```
curl -s http://localhost:8001/|jq .board|sed 's/\\n/\n/g'|sed 's/"//g'
```

# LICENSE

AGPL v3

sgf2images includes `static/p5.min.js` which is LGPL 2.1 <https://p5js.org/copyright.html>

The images in `static/*/*jpg` are from <https://www.godrago.net/WoodTextures.htm>
